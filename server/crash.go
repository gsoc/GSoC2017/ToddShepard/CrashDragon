package main

import (
	"html/template"
	"net/http"
	"strconv"
	"strings"

	"code.videolan.org/GSoC2017/ToddShepard/CrashDragon/database"
	"github.com/gin-gonic/gin"
	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday"
	uuid "github.com/satori/go.uuid"
)

// PostCrashComment allows you to post a comment to a crash
func PostCrashComment(c *gin.Context) {
	User := c.MustGet("user").(database.User)
	var Crash database.Crash
	database.Db.First(&Crash, "id = ?", c.Param("id"))
	if Crash.ID == uuid.Nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	var Comment database.Comment
	database.Db.FirstOrInit(&Comment)
	Comment.UserID = User.ID
	Comment.ID = uuid.NewV4()
	unsafe := blackfriday.MarkdownCommon([]byte(c.PostForm("comment")))
	Comment.Content = template.HTML(bluemonday.UGCPolicy().SanitizeBytes(unsafe))
	if len(strings.TrimSpace(string(Comment.Content))) == 0 {
		c.Redirect(http.StatusMovedPermanently, "/crashes/"+Crash.ID.String())
		return
	}
	Comment.ReportID = uuid.Nil
	Comment.CrashID = Crash.ID
	database.Db.Save(&Comment)
	c.Redirect(http.StatusMovedPermanently, "/crashes/"+Crash.ID.String()+"#comment-"+Comment.ID.String())
}

// GetCrashes returns crashes
func GetCrashes(c *gin.Context) {
	var Crashes []database.Crash
	query := database.Db
	all, prod := GetProductCookie(c)
	if !all {
		query = query.Where("product_id = ?", prod.ID)
	}
	offset, err := strconv.Atoi(c.DefaultQuery("offset", "0"))
	if err != nil {
		offset = 0
	}
	var count int
	query.Model(database.Crash{}).Count(&count)
	query.Order("created_at DESC").Offset(offset).Limit(50).Find(&Crashes)
	var next int
	var prev int
	if (offset + 50) >= count {
		next = -1
	} else {
		next = offset + 50
	}
	prev = offset - 50
	c.HTML(http.StatusOK, "crashes.html", gin.H{
		"prods":      database.Products,
		"title":      "Crashes",
		"items":      Crashes,
		"nextOffset": next,
		"prevOffset": prev,
	})
}

// GetCrash returns details of a crash
func GetCrash(c *gin.Context) {
	var Crash database.Crash
	database.Db.First(&Crash, "id = ?", c.Param("id"))
	database.Db.Model(&Crash).Preload("Product").Preload("Version").Related(&Crash.Reports)
	database.Db.Model(&Crash).Preload("User").Order("created_at ASC").Related(&Crash.Comments)
	c.HTML(http.StatusOK, "crash.html", gin.H{
		"prods":      database.Products,
		"detailView": true,
		"title":      "Crash",
		"items":      Crash.Reports,
		"comments":   Crash.Comments,
		"ID":         Crash.ID.String(),
	})
}
